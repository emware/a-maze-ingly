.PHONY: setup sh psalm usage test type-assertions ci cs-check cs-fix

usage:
	@echo "select target"

psalm:
	./vendor/bin/psalm --no-cache

test:
	./vendor/bin/phpunit --testsuite=unit

cs-fix:
	./vendor/bin/php-cs-fixer fix --ansi --verbose

cs-check:
	./vendor/bin/php-cs-fixer fix --ansi --verbose --dry-run

ci: test psalm cs-check
