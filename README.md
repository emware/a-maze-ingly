___A-Maze-ingly Retro Route Puzzle___

## Usage:
You should place your ***map.json*** in the folder `/mnt/data/`.

In order to run the run.sh entrypoint should be invoked with the following arguments
```bash
docker run -v $(pwd):/mnt -p 9090:9090 -w /mnt mytest ./scripts/run.sh --items=<Items> <StartRoomId>
```
eg:
```bash
docker run -v $(pwd):/mnt -p 9090:9090 -w /mnt mytest ./scripts/run.sh --items="Knife, Potted Plant, Chair" 2
```

## Requisites:
Docker is a mandatory technology to master. Each artifact must
1. contains a Dockerfile into the root directory
2. the full directory will be mounted under /mnt/ folder into the docker image builded from your docker file
3. if the implementation is for a network accessible service it must be binded to the port :9090
4. contains a build script runnable within the docker container generated from the Docker file named scripts/build.sh 5. contains a test script runnable within the docker container generated from the Docker file named scripts/test.sh 6. contains a run script runnable within the docker container generated from the Docker file named scripts/run.sh
   The candidate can simulate the review process with these commands, that must be run from the root of the project folder:

The candidate can simulate the review process with these commands, that must be run from the root of the project folder:

#### commands:
```
docker build -t mytest .
docker run -v $(pwd):/mnt -p 9090:9090 -w /mnt mytest ./scripts/build.sh 
docker run -v $(pwd):/mnt -p 9090:9090 -w /mnt mytest ./scripts/tests.sh 
docker run -v $(pwd):/mnt -p 9090:9090 -w /mnt mytest ./scripts/run.sh
```

## Problem:

Write a program that will output a valid route one could follow to collect all specified items within a map. The map is a json description of set of rooms with allowed path and contained object.

exercize starts with an input of:
- json reppresentation of map 
- starting room
- list of object to collect

```
Room type allowed fields
  id: Integer
  name: String
  north: Integer //referring to a connected room
  south: Integer //referring to a connected room
  west: Integer  //referring to a connected room
  east: Integer  //referring to a connected room
  objects: List  //of Objects
Object type allowed fields
  name: String //Object Name
```

## Goals:
TDD approach.

Build a Docker container with runnable code inside so that we can mount a volume in it and test on different maps.