<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Infrastructure\Handler;

use AMZ\Application\Explorer\Factory\ExplorerFactory;
use AMZ\Application\Maze\Factory\MapFactory;
use AMZ\Domain\Command\CollectObjects;
use AMZ\Domain\Diary\ExplorationDiary;
use AMZ\Domain\Explorer\ExplorerAggregate;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Map;
use AMZ\Domain\Maze\Room\Room;
use AMZ\Domain\Maze\Room\RoomId;
use AMZ\Domain\Maze\Room\RoomName;
use AMZ\Infrastructure\Adapter\MazeJsonFileReader;
use AMZ\Infrastructure\Handler\CollectObjectsHandler;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @covers \AMZ\Infrastructure\Handler\CollectObjectsHandler
 */
class ExplorerHandlerTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @test
     */
    public function shouldLoadFileAndCreateMap(): void
    {
        $path = './test';
        $data = ['test' => 123];
        $map = $this->prophesize(Map::class);
        $room = new Room(new RoomId(1), new RoomName('Bathroom'), new ItemList());
        $map->getRoom(Argument::any())->willReturn($room);

        $diary = $this->prophesize(ExplorationDiary::class);
        $mapFactory = $this->prophesize(MapFactory::class);
        $jsonFileReader = $this->prophesize(MazeJsonFileReader::class);
        $explorer = $this->prophesize(ExplorerAggregate::class);

        $explorerFactory = $this->prophesize(ExplorerFactory::class);
        $explorerFactory->make(Argument::cetera())->willReturn($explorer->reveal());

        $jsonFileReader->decodeFromPath($path)->willReturn($data)->shouldBeCalledOnce();
        $mapFactory->__invoke($data)->willReturn($map->reveal())->shouldBeCalledOnce();

        $handler = new CollectObjectsHandler($diary->reveal(), $mapFactory->reveal(), $jsonFileReader->reveal(), $explorerFactory->reveal(), $path);

        $command = new CollectObjects(new RoomId(1), new ItemList());

        ($handler)($command);
    }
}
