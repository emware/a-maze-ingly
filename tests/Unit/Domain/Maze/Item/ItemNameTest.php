<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Maze\Room;

use AMZ\Domain\Error\EmptyObjectName;
use AMZ\Domain\Maze\Item\ItemName;
use PHPUnit\Framework\TestCase;

/**
 * @covers \AMZ\Domain\Maze\Item\ObjectId
 */
class ItemNameTest extends TestCase
{
    /**
     * @test
     */
    public function shouldHonorContract(): void
    {
        $name = (string) uniqid('', true);
        $objectName = new ItemName($name);

        self::assertEquals($name, $objectName);
    }

    /**
     * @test
     */
    public function shouldOnlyAcceptNonEmptyString(): void
    {
        self::expectException(EmptyObjectName::class);
        new ItemName('');
    }
}
