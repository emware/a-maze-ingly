<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Maze\Item;

use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Item\ItemName;
use PHPUnit\Framework\TestCase;

/**
 * @covers ItemList
 */
class ItemListTest extends TestCase
{
    /**
     * @test
     */
    public function shouldContainsAListOfItems(): void
    {
        $object1 = new Item(new ItemName('Knife'));
        $object2 = new Item(new ItemName('Chair'));

        $list = new ItemList($object1, $object2);

        self::assertEquals([$object1, $object2], $list->value());
    }

    /**
     * @test
     */
    public function shouldCheckIfContainsObject(): void
    {
        $object1 = new Item(new ItemName('Knife'));
        $object2 = new Item(new ItemName('Chair'));
        $unknown = new Item(new ItemName('Black hole'));

        $list = new ItemList($object1, $object2);

        self::assertTrue($list->contains($object1));
        self::assertFalse($list->contains($unknown));
    }
}
