<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Maze\Item;

use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemName;
use PHPUnit\Framework\TestCase;

/**
 * @covers Item
 */
class ItemTest extends TestCase
{
    /**
     * @test
     */
    public function shouldRespectContract(): void
    {
        $name = 'Knife';

        $item = new Item(new ItemName($name));

        self::assertEquals($name, $item->value());
    }
}
