<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Maze\Room;

use AMZ\Domain\Error\InvalidRoomId;
use AMZ\Domain\Maze\Room\RoomId;
use PHPUnit\Framework\TestCase;

/**
 * @covers \AMZ\Domain\Maze\Room\RoomId
 */
class RoomIdTest extends TestCase
{
    /**
     * @test
     */
    public function shouldHonorContract(): void
    {
        $id = (int) uniqid('', true);
        $roomId = new RoomId($id);

        self::assertEquals($id, $roomId->value());
    }

    /**
     * @test
     */
    public function shouldOnlyAcceptNonEmptyString(): void
    {
        self::expectException(InvalidRoomId::class);
        new RoomId(-1);
    }
}
