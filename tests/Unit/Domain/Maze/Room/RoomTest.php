<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Maze\Room;

use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Item\ItemName;
use AMZ\Domain\Maze\Room\Room;
use AMZ\Domain\Maze\Room\RoomId;
use AMZ\Domain\Maze\Room\RoomName;
use PHPUnit\Framework\TestCase;

/**
 * @covers Room
 */
class RoomTest extends TestCase
{
    /**
     * @test
     */
    public function shouldRespectContract(): void
    {
        $id = 1;
        $name = 'Bathroom';
        $itemList = new ItemList(
            new Item(new ItemName('Knife')),
            new Item(new ItemName('Chair')),
        );

        $room = new Room(new RoomId($id), new RoomName($name), $itemList);

        self::assertEquals($id, $room->id()->value());
        self::assertEquals($name, $room->name()->value());
    }

    /**
     * @test
     */
    public function shouldCheckIfContainsObject(): void
    {
        $id = 1;
        $name = 'Bathroom';
        $itemList = new ItemList(
            new Item(new ItemName('Knife')),
            new Item(new ItemName('Chair')),
        );

        $room = new Room(new RoomId($id), new RoomName($name), $itemList);

        $wantedObject = new Item(new ItemName('Knife'));
        $missingObject = new Item(new ItemName('Black hole'));

        self::assertTrue($room->contains($wantedObject));
        self::assertFalse($room->contains($missingObject));
    }

    /**
     * @test
     */
    public function shouldHaveNoRoomsAround(): void
    {
        $id = 1;
        $name = 'Bathroom';
        $itemList = new ItemList();

        $room = new Room(new RoomId($id), new RoomName($name), $itemList);

        self::assertNull($room->getNorthRoom());
        self::assertNull($room->getEastRoom());
        self::assertNull($room->getSouthRoom());
        self::assertNull($room->getWestRoom());
    }

    /**
     * @test
     */
    public function shouldColocateNorthRoom(): void
    {
        $itemList = new ItemList();

        $room = new Room(new RoomId(1), new RoomName('Bathroom'), $itemList);
        $communicatingRoom = new Room(new RoomId(2), new RoomName('Kitchen'), $itemList);

        $room->setNorthRoom($communicatingRoom);

        self::assertSame($communicatingRoom, $room->getNorthRoom());
        self::assertSame($room, $communicatingRoom->getSouthRoom());
    }

    /**
     * @test
     */
    public function shouldColocateWestRoom(): void
    {
        $itemList = new ItemList();

        $room = new Room(new RoomId(1), new RoomName('Bathroom'), $itemList);
        $communicatingRoom = new Room(new RoomId(2), new RoomName('Kitchen'), $itemList);

        $room->setWestRoom($communicatingRoom);

        self::assertSame($communicatingRoom, $room->getWestRoom());
        self::assertSame($room, $communicatingRoom->getEastRoom());
    }
}
