<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Maze\Room;

use AMZ\Domain\Error\EmptyRoomName;
use AMZ\Domain\Maze\Room\RoomName;
use PHPUnit\Framework\TestCase;

/**
 * @covers \AMZ\Domain\Maze\Room\RoomName
 */
class RoomNameTest extends TestCase
{
    /**
     * @test
     */
    public function shouldHonorContract(): void
    {
        $name = (string) uniqid('name', true);
        $roomName = new RoomName($name);

        self::assertEquals($name, $roomName);
    }

    /**
     * @test
     */
    public function shouldOnlyAcceptNonEmptyString(): void
    {
        self::expectException(EmptyRoomName::class);
        new RoomName('');
    }
}
