<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Domain\Explorer;

use AMZ\Application\Explorer\Factory\ExplorerFactory;
use AMZ\Application\Maze\Factory\MakeMapFromArray;
use AMZ\Domain\Diary\EphemeralExplorationDiary;
use AMZ\Domain\Event\Init;
use AMZ\Domain\Event\ItemCollected;
use AMZ\Domain\Event\RoomTraversed;
use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Item\ItemName;
use AMZ\Domain\Maze\Map;
use AMZ\Domain\Maze\Room\RoomId;
use AMZ\Domain\Maze\Room\RoomName;
use PHPUnit\Framework\TestCase;

/**
 * @covers \AMZ\Domain\Explorer\Explorer
 */
class ExplorerTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCollectObject(): void
    {
        $map = $this->makeMap([[
            'id' => 1,
            'name' => 'Bathroom',
            'objects' => ['Knife'],
        ]]);

        $diary = new EphemeralExplorationDiary();

        $explorer = (new ExplorerFactory())->make($diary, $map, new Init(new RoomId(1)));

        $explorer->startCollectingItems(new ItemList(new Item(new ItemName('Knife'))));

        self::assertEquals([
            new Init(new RoomId(1)),
            new ItemCollected(new RoomId(1), new RoomName('Bathroom'), new Item(new ItemName('Knife'))),
        ], $diary->getAllEvents());
    }

    /**
     * @test
     */
    public function shouldNotCollectObjectIfNotPresent(): void
    {
        $map = $this->makeMap([[
            'id' => 1,
            'name' => 'Bathroom',
            'objects' => ['Knife'],
        ]]);

        $diary = new EphemeralExplorationDiary();

        $explorer = (new ExplorerFactory())->make($diary, $map, new Init(new RoomId(1)));

        $explorer->startCollectingItems(new ItemList(new Item(new ItemName('Black hole'))));

        self::assertEquals([
            new Init(new RoomId(1)),
            new RoomTraversed(new RoomId(1), new RoomName('Bathroom')),
        ], $diary->getAllEvents());
    }

    /**
     * @test
     */
    public function shouldShouldMoveToNearRoom(): void
    {
        $map = $this->makeMap([
            [
                'id' => 1,
                'name' => 'Bathroom',
                'objects' => ['Knife'],
                'east' => 2,
            ],
            [
                'id' => 2,
                'name' => 'Kitchen',
                'objects' => ['Cup'],
                'west' => 1,
            ],
        ]);

        $diary = new EphemeralExplorationDiary();

        $explorer = (new ExplorerFactory())->make($diary, $map, new Init(new RoomId(1)));

        $explorer->startCollectingItems(new ItemList(new Item(new ItemName('Knife')), new Item(new ItemName('Cup'))));

        self::assertEquals([
            new Init(new RoomId(1)),
            new ItemCollected(new RoomId(1), new RoomName('Bathroom'), new Item(new ItemName('Knife'))),
            new ItemCollected(new RoomId(2), new RoomName('Kitchen'), new Item(new ItemName('Cup'))),
        ], $diary->getAllEvents());
    }

    /**
     * @test
     */
    public function shouldCrossNearRoom(): void
    {
        $map = $this->makeMap([
            [
                'id' => 1,
                'name' => 'Bathroom',
                'objects' => ['Knife'],
                'east' => 2,
            ],
            [
                'id' => 2,
                'name' => 'Kitchen',
                'objects' => ['Cup'],
                'west' => 1,
            ],
        ]);

        $diary = new EphemeralExplorationDiary();

        $explorer = (new ExplorerFactory())->make($diary, $map, new Init(new RoomId(1)));

        $explorer->startCollectingItems(new ItemList(new Item(new ItemName('Knife')), new Item(new ItemName('Cup'))));

        self::assertEquals([
            new Init(new RoomId(1)),
            new ItemCollected(new RoomId(1), new RoomName('Bathroom'), new Item(new ItemName('Knife'))),
            new ItemCollected(new RoomId(2), new RoomName('Kitchen'), new Item(new ItemName('Cup'))),
        ], $diary->getAllEvents());
    }

    /**
     * @test
     */
    public function shouldBacktrackToUnvisitedRooms(): void
    {
        $map = $this->makeMap([
            [
                'id' => 1,
                'name' => 'Bathroom',
                'objects' => ['Knife'],
                'east' => 2,
            ],
            [
                'id' => 2,
                'name' => 'Kitchen',
                'objects' => ['Cup'],
                'west' => 1,
                'east' => 3,
                'north' => 4,
            ],
            [
                'id' => 3,
                'name' => 'Dinning Room',
                'objects' => ['Potted Plant'],
                'west' => 2,
            ],
            [
                'id' => 4,
                'name' => 'Sun Room',
                'objects' => ['Chair'],
                'south' => 2,
            ],
        ]);

        $diary = new EphemeralExplorationDiary();

        $explorer = (new ExplorerFactory())->make($diary, $map, new Init(new RoomId(1)));

        $explorer->startCollectingItems(new ItemList(
            new Item(new ItemName('Knife')),
            new Item(new ItemName('Cup')),
            new Item(new ItemName('Potted Plant')),
            new Item(new ItemName('Chair')),
        ));

        self::assertEquals([
            new Init(new RoomId(1)),
            new ItemCollected(new RoomId(1), new RoomName('Bathroom'), new Item(new ItemName('Knife'))),
            new ItemCollected(new RoomId(2), new RoomName('Kitchen'), new Item(new ItemName('Cup'))),
            new ItemCollected(new RoomId(3), new RoomName('Dinning Room'), new Item(new ItemName('Potted Plant'))),
            new RoomTraversed(new RoomId(2), new RoomName('Kitchen')),
            new ItemCollected(new RoomId(4), new RoomName('Sun Room'), new Item(new ItemName('Chair'))),
        ], $diary->getAllEvents());
    }

    private function makeMap(array $data): Map
    {
        return (new MakeMapFromArray())($data);
    }
}
