<?php

declare(strict_types=1);

namespace AMZ\Tests\Unit\Application\Maze;

use AMZ\Application\Maze\Factory\MakeMapFromArray;
use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Item\ItemName;
use AMZ\Domain\Maze\Room\Room;
use AMZ\Domain\Maze\Room\RoomId;
use PHPUnit\Framework\TestCase;

/**
 * @covers \AMZ\Application\Maze\Factory\MakeMapFromArray
 */
class MapFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function shouldCreateSingleRoomMap(): void
    {
        $input = [
            [
                'id' => 1,
                'name' => 'Hallway',
                'objects' => [],
            ],
        ];

        $map = (new MakeMapFromArray())($input);

        self::assertInstanceOf(Room::class, $map->getRoom(new RoomId(1)));
    }

    /**
     * @test
     */
    public function shouldCreateAdjacentRooms(): void
    {
        $input = [
            [
                'id' => 1,
                'name' => 'Hallway',
                'objects' => [],
                'east' => 2,
                'south' => 3,
            ],
            [
                'id' => 2,
                'name' => 'Dining Room',
                'objects' => [],
                'west' => 1,
                'south' => 4,
            ],
            [
                'id' => 3,
                'name' => 'Kitchen',
                'objects' => [],
                'east' => 4,
                'north' => 1,
            ],
            [
                'id' => 4,
                'name' => 'Sun Room',
                'objects' => [],
                'west' => 3,
                'north' => 2,
            ],
        ];

        $map = (new MakeMapFromArray())($input);

        $room1 = $map->getRoom(new RoomId(1));
        $room2 = $map->getRoom(new RoomId(2));
        $room3 = $map->getRoom(new RoomId(3));
        $room4 = $map->getRoom(new RoomId(4));

        self::assertEquals(2, $room1->getEastRoom()->id()->value());
        self::assertEquals(3, $room1->getSouthRoom()->id()->value());
        self::assertEquals(1, $room2->getWestRoom()->id()->value());
        self::assertEquals(4, $room2->getSouthRoom()->id()->value());
        self::assertEquals(1, $room3->getNorthRoom()->id()->value());
        self::assertEquals(4, $room3->getEastRoom()->id()->value());
        self::assertEquals(2, $room4->getNorthRoom()->id()->value());
        self::assertEquals(3, $room4->getWestRoom()->id()->value());
    }

    /**
     * @test
     */
    public function shouldCreateRoomWithItems(): void
    {
        $input = [
            [
                'id' => 1,
                'name' => 'Hallway',
                'objects' => ['Knife', 'Bell'],
            ],
        ];

        $map = (new MakeMapFromArray())($input);

        self::assertEquals(
            new ItemList(
                new Item(new ItemName('Knife')),
                new Item(new ItemName('Bell')),
            ),
            $map->getRoom(new RoomId(1))->itemList()
        );
    }
}
