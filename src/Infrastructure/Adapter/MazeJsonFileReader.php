<?php

declare(strict_types=1);

namespace AMZ\Infrastructure\Adapter;

class MazeJsonFileReader
{
    /**
     * @param string $path
     *
     * @return array<array{id: int, name: string, objects: array<string>, east?: int, west?: int, north?: int, south?: int}>
     */
    public function decodeFromPath(string $path): array
    {
        $fileContent = file_get_contents(__DIR__ . '/../../../' . $path);

        /** @var array{rooms: array<array{id: int, name: string, objects: array<string>, east?: int, west?: int, north?: int, south?: int}>} $data */
        $data = \json_decode($fileContent, true, 512, \JSON_THROW_ON_ERROR);

        return $data['rooms'];
    }
}
