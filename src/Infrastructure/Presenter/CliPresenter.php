<?php

declare(strict_types=1);

namespace AMZ\Infrastructure\Presenter;

use AMZ\Domain\View\ExplorationLogView;
use Symfony\Component\Console\Output\OutputInterface;

class CliPresenter
{
    public function __construct(private OutputInterface $output)
    {
    }

    public function __invoke(ExplorationLogView $view): void
    {
        $this->printHeader();

        $this->printBody($view);
    }

    /**
     * @param array{id: int, name: string, object: string} $data
     *
     * @return string
     */
    private function formatLine(array $data): string
    {
        return sprintf('%-8s%-16s%s', $data['id'], $data['name'], $data['object']);
    }

    private function printHeader(): void
    {
        $this->output->writeln('ID' . "\t" . 'Room' . "\t\t" . 'Object collected');
        $this->output->writeln('----------------------------------------');
    }

    private function printBody(ExplorationLogView $view): void
    {
        foreach ($view->getRawData() as $rawDatum) {
            $line = $this->formatLine($rawDatum);
            $this->output->writeln($line);
        }
    }
}
