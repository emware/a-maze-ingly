<?php

declare(strict_types=1);

namespace AMZ\Infrastructure\Handler;

use AMZ\Application\Explorer\Factory\ExplorerFactory;
use AMZ\Application\Maze\Factory\MakeMapFromArray;
use AMZ\Domain\Diary\ExplorationDiary;
use AMZ\Infrastructure\Adapter\MazeJsonFileReader;

class ExploreMazeHandlerFactory
{
    public static function make(ExplorationDiary $diary): CollectObjectsHandler
    {
        return new CollectObjectsHandler($diary, new MakeMapFromArray(), new MazeJsonFileReader(), new ExplorerFactory(), './data/map.json');
    }
}
