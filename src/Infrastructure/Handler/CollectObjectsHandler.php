<?php

declare(strict_types=1);

namespace AMZ\Infrastructure\Handler;

use AMZ\Application\Explorer\Factory\ExplorerFactory;
use AMZ\Application\Maze\Factory\MapFactory;
use AMZ\Domain\Command\CollectObjects;
use AMZ\Domain\Command\Handler\CollectObjectsHandler as ICollectObjectsHandler;
use AMZ\Domain\Diary\ExplorationDiary;
use AMZ\Domain\Event\Init;
use AMZ\Infrastructure\Adapter\MazeJsonFileReader;

final class CollectObjectsHandler implements ICollectObjectsHandler
{
    public function __construct(
        private ExplorationDiary $diary,
        private MapFactory $mapFactory,
        private MazeJsonFileReader $jsonFileReader,
        private ExplorerFactory $explorerFactory,
        private string $mazeJsonPath
    ) {
    }

    public function __invoke(CollectObjects $command): void
    {
        $mazeData = $this->jsonFileReader->decodeFromPath($this->mazeJsonPath);
        $map = ($this->mapFactory)($mazeData);

        $explorer = $this->explorerFactory->make($this->diary, $map, new Init($command->initialRoomId()));

        $explorer->startCollectingItems($command->requiredItems());
    }
}
