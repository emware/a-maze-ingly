<?php

declare(strict_types=1);

namespace AMZ;

require './vendor/autoload.php';

use AMZ\Application\Maze\Factory\ItemListFactory;
use AMZ\Domain\Command\CollectObjects;
use AMZ\Domain\Diary\EphemeralExplorationDiary;
use AMZ\Domain\Maze\Room\RoomId;
use AMZ\Domain\View\ExplorationLogView;
use AMZ\Infrastructure\Handler\ExploreMazeHandlerFactory;
use AMZ\Infrastructure\Presenter\CliPresenter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\SingleCommandApplication;

(new SingleCommandApplication())
    ->setName('Amazeingly')
    ->setVersion('1.0.0')
    ->addArgument('roomId', InputArgument::REQUIRED, 'Initial Room id')
    ->addOption('items', 'i', InputOption::VALUE_REQUIRED)
    ->setCode(static function (InputInterface $input, OutputInterface $output) {
        /** @psalm-suppress PossiblyInvalidCast */
        $items = array_map('trim', explode(',', (string) $input->getOption('items')));
        $roomId = new RoomId((int) $input->getArgument('roomId'));

        $diary = new EphemeralExplorationDiary();

        $handler = ExploreMazeHandlerFactory::make($diary);

        $itemList = (new ItemListFactory())($items);
        $command = new CollectObjects($roomId, $itemList);

        ($handler)($command);

        $view = new ExplorationLogView($diary);

        (new CliPresenter($output))($view);
    })
    ->run();
