<?php

declare(strict_types=1);

namespace AMZ\Domain\View;

use AMZ\Domain\Diary\ExplorationDiary;
use AMZ\Domain\Event\ExplorationEvent;
use AMZ\Domain\Event\ItemCollected;
use AMZ\Domain\Event\RoomTraversed;

final class ExplorationLogView
{
    /**
     * @var array<array{id: int, name: string, object: string}>
     */
    private array $explorationLog = [];

    public function __construct(ExplorationDiary $diary)
    {
        foreach ($diary->getAllEvents() as $event) {
            $this->apply($event);
        }
    }

    public function apply(ExplorationEvent $event): void
    {
        if ($event instanceof ItemCollected) {
            $this->explorationLog[] = [
                'id' => $event->roomId()->value(),
                'name' => $event->roomName()->value(),
                'object' => $event->item()->name()->value(),
            ];
        }

        if ($event instanceof RoomTraversed) {
            $this->explorationLog[] = [
                'id' => $event->roomId()->value(),
                'name' => $event->roomName()->value(),
                'object' => 'None',
            ];
        }
    }

    /**
     * @return array<array{id: int, name: string, object: string}>
     */
    public function getRawData(): array
    {
        return $this->explorationLog;
    }
}
