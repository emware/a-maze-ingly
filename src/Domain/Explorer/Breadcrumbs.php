<?php

declare(strict_types=1);

namespace AMZ\Domain\Explorer;

use AMZ\Domain\Maze\Room\Room;

class Breadcrumbs
{
    private \SplStack $stack;

    public function __construct()
    {
        $this->stack = new \SplStack();
    }

    public function addRoom(Room $room): void
    {
        $this->stack->push($room);
    }

    public function getPrevious(): ?Room
    {
        /** @var Room $room */
        $room = $this->stack->pop();

        return $room;
    }

    public function isEmpty(): bool
    {
        return $this->stack->isEmpty();
    }
}
