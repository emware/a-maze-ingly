<?php

declare(strict_types=1);

namespace AMZ\Domain\Explorer;

use AMZ\Domain\Maze\Item\ItemList;

interface ExplorerAggregate
{
    public function startCollectingItems(ItemList $items): void;
}
