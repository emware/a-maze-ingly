<?php

declare(strict_types=1);

namespace AMZ\Domain\Explorer;

use AMZ\Domain\Event\ExplorationEvent;
use AMZ\Domain\Event\Init;
use AMZ\Domain\Event\ItemCollected;
use AMZ\Domain\Event\RoomTraversed;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Map;
use AMZ\Domain\Maze\Room\RoomId;

final class ExplorerState
{
    private RoomId $currentRoomId;

    private ItemList $collectedItems;

    /**
     * @var array<int, RoomId>
     */
    private array $visitedRooms = [];

    public function __construct()
    {
        $this->collectedItems = new ItemList();
        $this->currentRoomId = new RoomId(0);
    }

    public function apply(ExplorationEvent $event): void
    {
        if ($event instanceof Init) {
            $this->currentRoomId = $event->roomId();
        } elseif ($event instanceof ItemCollected) {
            $this->currentRoomId = $event->roomId();
            $this->collectedItems = $this->collectedItems->add($event->item());
            $this->visitedRooms[$event->roomId()->value()] = $event->roomId();
        } elseif ($event instanceof RoomTraversed) {
            $this->currentRoomId = $event->roomId();
            $this->visitedRooms[$event->roomId()->value()] = $event->roomId();
        }
    }

    public function currentRoomId(): RoomId
    {
        return $this->currentRoomId;
    }

    public function missingItems(ItemList $desiredItemsList): ItemList
    {
        return $desiredItemsList->removeItems($this->collectedItems);
    }

    /**
     * @param Map $map
     *
     * @return bool
     */
    public function isMapFullyExplored(Map $map): bool
    {
        foreach ($map->rooms() as $room) {
            if (! isset($this->visitedRooms[$room->id()->value()])) {
                return false;
            }
        }

        return true;
    }

    public function isRoomExplored(RoomId $roomId): bool
    {
        return isset($this->visitedRooms[$roomId->value()]);
    }
}
