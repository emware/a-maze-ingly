<?php

declare(strict_types=1);

namespace AMZ\Domain\Explorer;

use AMZ\Domain\Event\ExplorationEvent;
use AMZ\Domain\Event\ItemCollected;
use AMZ\Domain\Event\RoomTraversed;
use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Map;
use AMZ\Domain\Maze\Room\Room;

class Explorer implements ExplorerAggregate
{
    /**
     * @var callable(ExplorationEvent)
     */
    private $writeEvent;

    private Breadcrumbs $breadcrumbs;

    /**
     * @param ExplorerState $state
     * @param Map $map
     * @param callable(ExplorationEvent $event) $writeEvent
     */
    public function __construct(private ExplorerState $state, private Map $map, callable $writeEvent)
    {
        $this->writeEvent = $writeEvent;
        $this->breadcrumbs = new Breadcrumbs();
    }

    public function startCollectingItems(ItemList $items): void
    {
        $currentRoom = $this->getCurrentRoom();
        $this->explore($currentRoom, $items);
    }

    public function explore(Room $room, ItemList $toCollectItems): void
    {
        $missingItems = $this->collectAllItemsFromRoom($room, $toCollectItems);

        if (! $missingItems->value() || $this->state->isMapFullyExplored($this->map)) {
            return;
        }

        $this->exploreNextRoom($room, $missingItems);
    }

    private function collectAllItemsFromRoom(Room $room, ItemList $itemList): ItemList
    {
        $itemsCollected = 0;
        foreach ($itemList->value() as $item) {
            $event = $this->collectFromRoom($room, $item);
            if ($event) {
                $itemsCollected++;
                ($this->writeEvent)($event);
            }
        }

        if (0 === $itemsCollected) {
            ($this->writeEvent)(new RoomTraversed($room->id(), $room->name()));
        }

        return $this->state->missingItems($itemList);
    }

    private function collectFromRoom(Room $room, Item $item): ?ItemCollected
    {
        return $room->contains($item)
            ? new ItemCollected($room->id(), $room->name(), $item)
            : null;
    }

    private function getCurrentRoom(): Room
    {
        $currentRoomId = $this->state->currentRoomId();

        return $this->map->getRoom($currentRoomId);
    }

    /**
     * @param Room $currentRoom
     *
     * @return Room[]
     */
    private function getUnvisitedConnectedRooms(Room $currentRoom): array
    {
        $connectedRooms = $currentRoom->getConnectedRooms();

        return array_filter($connectedRooms, fn (Room $room) => ! $this->state->isRoomExplored($room->id()));
    }

    /**
     * @param ItemList $missingItems
     */
    private function backtrackToPreviousRoom(ItemList $missingItems): void
    {
        if ($this->breadcrumbs->isEmpty()) {
            return;
        }

        $backtrackRoom = $this->breadcrumbs->getPrevious();

        if (! $backtrackRoom) {
            return;
        }

        $this->explore($backtrackRoom, $missingItems);
    }

    private function addBreadcrumbs(array $unvisitedConnectedRooms, Room $room): void
    {
        if (! $this->breadcrumbs->isEmpty() || count($unvisitedConnectedRooms) > 1) {
            $this->breadcrumbs->addRoom($room);
        }
    }

    private function exploreNextRoom(Room $room, ItemList $missingItems): void
    {
        $unvisitedConnectedRooms = $this->getUnvisitedConnectedRooms($room);

        if (! $unvisitedConnectedRooms) {
            $this->backtrackToPreviousRoom($missingItems);

            return;
        }

        $this->addBreadcrumbs($unvisitedConnectedRooms, $room);

        $this->explore(array_shift($unvisitedConnectedRooms), $missingItems);
    }
}
