<?php

declare(strict_types=1);

namespace AMZ\Domain;

/**
 * @template T
 */
interface ValueObject
{
    public function equals(self $object): bool;

    /**
     * @return T
     */
    public function value();
}
