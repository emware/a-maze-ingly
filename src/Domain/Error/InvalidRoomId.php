<?php

declare(strict_types=1);

namespace AMZ\Domain\Error;

final class InvalidRoomId extends \DomainException
{
    public function __construct(int $value)
    {
        parent::__construct(sprintf('Invalid RoomId: %s', $value));
    }
}
