<?php

declare(strict_types=1);

namespace AMZ\Domain\Error;

final class EmptyObjectName extends \DomainException
{
    public function __construct()
    {
        parent::__construct(sprintf('ObjectName should not be empty'));
    }
}
