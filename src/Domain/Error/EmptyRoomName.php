<?php

declare(strict_types=1);

namespace AMZ\Domain\Error;

final class EmptyRoomName extends \DomainException
{
    public function __construct()
    {
        parent::__construct(sprintf('RoomName should not be empty'));
    }
}
