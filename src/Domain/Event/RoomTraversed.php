<?php

declare(strict_types=1);

namespace AMZ\Domain\Event;

use AMZ\Domain\Maze\Room\RoomId;
use AMZ\Domain\Maze\Room\RoomName;

final class RoomTraversed implements ExplorationEvent
{
    public function __construct(private RoomId $roomId, private RoomName $roomName)
    {
    }

    public function roomId(): RoomId
    {
        return $this->roomId;
    }

    public function roomName(): RoomName
    {
        return $this->roomName;
    }
}
