<?php

declare(strict_types=1);

namespace AMZ\Domain\Event;

use AMZ\Domain\Maze\Room\RoomId;

final class Init implements ExplorationEvent
{
    public function __construct(private RoomId $roomId)
    {
    }

    public function roomId(): RoomId
    {
        return $this->roomId;
    }
}
