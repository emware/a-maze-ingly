<?php

declare(strict_types=1);

namespace AMZ\Domain\Diary;

use AMZ\Domain\Event\ExplorationEvent;

interface ExplorationDiary
{
    public function addEvent(ExplorationEvent $event): void;

    /**
     * @return array<ExplorationEvent>
     */
    public function getAllEvents(): array;
}
