<?php

declare(strict_types=1);

namespace AMZ\Domain\Diary;

use AMZ\Domain\Event\ExplorationEvent;

final class EphemeralExplorationDiary implements ExplorationDiary
{
    /**
     * @param array<ExplorationEvent> $events
     */
    public function __construct(private array $events = [])
    {
    }

    public function addEvent(ExplorationEvent $event): void
    {
        $this->events[] = $event;
    }

    public function getAllEvents(): array
    {
        return $this->events;
    }
}
