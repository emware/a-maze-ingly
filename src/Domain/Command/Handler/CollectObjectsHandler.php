<?php

declare(strict_types=1);

namespace AMZ\Domain\Command\Handler;

use AMZ\Domain\Command\CollectObjects;

interface CollectObjectsHandler
{
    public function __invoke(CollectObjects $command): void;
}
