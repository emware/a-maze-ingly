<?php

declare(strict_types=1);

namespace AMZ\Domain\Command;

use AMZ\Domain\Maze\Item\ItemList;
use AMZ\Domain\Maze\Room\RoomId;

final class CollectObjects implements Command
{
    public function __construct(private RoomId $fromRoomId, private ItemList $items)
    {
    }

    public function initialRoomId(): RoomId
    {
        return $this->fromRoomId;
    }

    public function requiredItems(): ItemList
    {
        return $this->items;
    }
}
