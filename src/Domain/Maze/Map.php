<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze;

use AMZ\Domain\Maze\Room\Room;
use AMZ\Domain\Maze\Room\RoomId;

class Map
{
    /**
     * @var Room[]
     */
    private array $rooms = [];

    public function __construct(Room ...$rooms)
    {
        foreach ($rooms as $room) {
            $this->rooms[$room->id()->value()] = $room;
        }
    }

    public function getRoom(RoomId $id): Room
    {
        $room = $this->rooms[$id->value()] ?? null;

        if (null === $room) {
            throw new \OutOfBoundsException("Room with id {$id->value()} not found");
        }

        return $room;
    }

    /**
     * @return Room[]
     */
    public function rooms(): array
    {
        return $this->rooms;
    }
}
