<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze\Room;

use AMZ\Domain\Error\InvalidRoomId;
use AMZ\Domain\ValueObject;

/**
 * @implements ValueObject<int>
 */
final class RoomId implements ValueObject
{
    public function __construct(private int $value)
    {
        if ($this->value < 0) {
            throw new InvalidRoomId($this->value);
        }
    }

    public function value(): int
    {
        return $this->value;
    }

    public function equals(ValueObject $object): bool
    {
        return $object->value() === $this->value;
    }
}
