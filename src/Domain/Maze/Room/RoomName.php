<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze\Room;

use AMZ\Domain\Error\EmptyRoomName;
use AMZ\Domain\ValueObject;

/**
 * @implements ValueObject<string>
 */
final class RoomName implements ValueObject
{
    public function __construct(private string $value)
    {
        if (strlen($this->value) <= 0) {
            throw new EmptyRoomName();
        }
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(ValueObject $object): bool
    {
        return $object->value() === $this->value;
    }

    public function __toString(): string
    {
        return $this->value();
    }
}
