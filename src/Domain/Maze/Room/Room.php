<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze\Room;

use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemList;

final class Room
{
    public function __construct(
        private RoomId $id,
        private RoomName $name,
        private ItemList $itemList,
        private Room | null $northRoom = null,
        private Room | null $eastRoom = null,
        private Room | null $southRoom = null,
        private Room | null $westRoom = null,
    ) {
    }

    public function id(): RoomId
    {
        return $this->id;
    }

    public function name(): RoomName
    {
        return $this->name;
    }

    public function itemList(): ItemList
    {
        return $this->itemList;
    }

    public function contains(Item $item): bool
    {
        return $this->itemList->contains($item);
    }

    public function getNorthRoom(): ?Room
    {
        return $this->northRoom;
    }

    public function getEastRoom(): ?Room
    {
        return $this->eastRoom;
    }

    public function getSouthRoom(): ?Room
    {
        return $this->southRoom;
    }

    public function getWestRoom(): ?Room
    {
        return $this->westRoom;
    }

    public function setNorthRoom(Room $room): void
    {
        $this->northRoom = $room;

        if ($room->getSouthRoom() !== $this) {
            $room->setSouthRoom($this);
        }
    }

    public function setSouthRoom(Room $room): void
    {
        $this->southRoom = $room;

        if ($room->getNorthRoom() !== $this) {
            $room->setNorthRoom($this);
        }
    }

    public function setWestRoom(Room $room): void
    {
        $this->westRoom = $room;

        if ($room->getEastRoom() !== $this) {
            $room->setEastRoom($this);
        }
    }

    public function setEastRoom(Room $room): void
    {
        $this->eastRoom = $room;

        if ($room->getWestRoom() !== $this) {
            $room->setWestRoom($this);
        }
    }

    /**
     * @return Room[]
     */
    public function getConnectedRooms(): array
    {
        return array_filter([$this->westRoom, $this->eastRoom, $this->southRoom, $this->northRoom]);
    }
}
