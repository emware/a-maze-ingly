<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze\Item;

use AMZ\Domain\ValueObject;

final class ItemList implements ValueObject
{
    /**
     * @var array<array-key, Item>
     */
    private array $items;

    public function __construct(Item ...$items)
    {
        $this->items = $items;
    }

    /**
     * @return array<Item>
     */
    public function value(): array
    {
        return $this->items;
    }

    public function contains(Item $item): bool
    {
        $foundItems = array_filter($this->items, static fn (Item $containedItems) => $containedItems->equals($item));

        return count($foundItems) > 0;
    }

    public function equals(ValueObject $object): bool
    {
        return $object->value() == $this->value();
    }

    public function add(Item $item): ItemList
    {
        $newItems = $this->items;
        $newItems[] = $item;

        return new self(...$newItems);
    }

    public function removeItems(ItemList $items): ItemList
    {
        $toRemoveNames = array_map(static fn (Item $item) => $item->name()->value(), $items->value());

        return new self(...array_filter($this->items, static fn (Item $currentItem) => ! in_array($currentItem->name()->value(), $toRemoveNames)));
    }
}
