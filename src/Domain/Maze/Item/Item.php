<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze\Item;

use AMZ\Domain\ValueObject;

/**
 * This should have been called Object, but for PHP limitation it is forbidden
 *
 * @implements ValueObject<ItemName>
 */
final class Item implements ValueObject
{
    public function __construct(private ItemName $name)
    {
    }

    public function equals(ValueObject $object): bool
    {
        if (! $object instanceof self) {
            return false;
        }

        return $object->name()->value() === $this->name->value();
    }

    public function name(): ItemName
    {
        return $this->name;
    }

    public function value(): ItemName
    {
        return $this->name;
    }
}
