<?php

declare(strict_types=1);

namespace AMZ\Domain\Maze\Item;

use AMZ\Domain\Error\EmptyObjectName;
use AMZ\Domain\ValueObject;

/**
 * @implements ValueObject<string>
 */
final class ItemName implements ValueObject
{
    public function __construct(private string $value)
    {
        if (strlen($this->value) <= 0) {
            throw new EmptyObjectName();
        }
    }

    public function value(): string
    {
        return $this->value;
    }

    public function equals(ValueObject $object): bool
    {
        return $object->value() === $this->value;
    }

    public function __toString(): string
    {
        return $this->value();
    }
}
