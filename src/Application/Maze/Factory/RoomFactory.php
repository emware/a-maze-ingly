<?php

declare(strict_types=1);

namespace AMZ\Application\Maze\Factory;

use AMZ\Domain\Maze\Room\Room;
use AMZ\Domain\Maze\Room\RoomId;
use AMZ\Domain\Maze\Room\RoomName;

final class RoomFactory
{
    /**
     * @param array{id: int, name: string, objects: array<string>} $data
     *
     * @return Room
     */
    public function __invoke(array $data): Room
    {
        return new Room(
            new RoomId($data['id']),
            new RoomName($data['name']),
            (new ItemListFactory())($data['objects'])
        );
    }
}
