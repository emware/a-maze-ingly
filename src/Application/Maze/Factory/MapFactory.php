<?php

declare(strict_types=1);

namespace AMZ\Application\Maze\Factory;

use AMZ\Domain\Maze\Map;

interface MapFactory
{
    /**
     * @param array<array{id: int, name: string, objects: array<string>, east?: int, west?: int, north?: int, south?: int}> $input
     *
     * @return Map
     */
    public function __invoke(array $input): Map;
}
