<?php

declare(strict_types=1);

namespace AMZ\Application\Maze\Factory;

use AMZ\Domain\Maze\Item\ItemList;

final class ItemListFactory
{
    /**
     * @param array<array{name: string} | string> $data
     *
     * @return ItemList
     */
    public function __invoke(array $data): ItemList
    {
        // @psalm-suppress MixedArgument
        return new ItemList(...array_map(static fn ($data) => (new ItemFactory())(is_array($data) ? $data['name'] : $data), $data));
    }
}
