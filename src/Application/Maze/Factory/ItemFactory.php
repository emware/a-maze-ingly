<?php

declare(strict_types=1);

namespace AMZ\Application\Maze\Factory;

use AMZ\Domain\Maze\Item\Item;
use AMZ\Domain\Maze\Item\ItemName;

final class ItemFactory
{
    public function __invoke(string $name): Item
    {
        return new Item(new ItemName($name));
    }
}
