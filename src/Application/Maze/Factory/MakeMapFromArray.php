<?php

declare(strict_types=1);

namespace AMZ\Application\Maze\Factory;

use AMZ\Domain\Maze\Map;
use AMZ\Domain\Maze\Room\RoomId;

final class MakeMapFromArray implements MapFactory
{
    /**
     * @param array<array{id: int, name: string, objects: array<string>, east?: int, west?: int, north?: int, south?: int}> $input
     *
     * @return Map
     */
    public function __invoke(array $input): Map
    {
        /** @psalm-suppress MixedArgument */
        $map = new Map(...array_map(new RoomFactory(), $input));

        foreach ($input as $roomDefinition) {
            if (isset($roomDefinition['east'])) {
                self::attachEast($map, $roomDefinition['id'], $roomDefinition['east']);
            }

            if (isset($roomDefinition['west'])) {
                self::attachWest($map, $roomDefinition['id'], $roomDefinition['west']);
            }

            if (isset($roomDefinition['north'])) {
                self::attachNorth($map, $roomDefinition['id'], $roomDefinition['north']);
            }

            if (isset($roomDefinition['south'])) {
                self::attachSouth($map, $roomDefinition['id'], $roomDefinition['south']);
            }
        }

        return $map;
    }

    private static function attachEast(Map $map, int $source, int $destination): void
    {
        $sourceRoom = $map->getRoom(new RoomId($source));

        $sourceRoom->setEastRoom($map->getRoom(new RoomId($destination)));
    }

    private static function attachWest(Map $map, int $source, int $destination): void
    {
        $sourceRoom = $map->getRoom(new RoomId($source));

        $sourceRoom->setWestRoom($map->getRoom(new RoomId($destination)));
    }

    private static function attachNorth(Map $map, int $source, int $destination): void
    {
        $sourceRoom = $map->getRoom(new RoomId($source));

        $sourceRoom->setNorthRoom($map->getRoom(new RoomId($destination)));
    }

    private static function attachSouth(Map $map, int $source, int $destination): void
    {
        $sourceRoom = $map->getRoom(new RoomId($source));

        $sourceRoom->setSouthRoom($map->getRoom(new RoomId($destination)));
    }
}
