<?php

declare(strict_types=1);

namespace AMZ\Application\Explorer\Factory;

use AMZ\Domain\Diary\ExplorationDiary;
use AMZ\Domain\Event\ExplorationEvent;
use AMZ\Domain\Explorer\Explorer;
use AMZ\Domain\Explorer\ExplorerAggregate;
use AMZ\Domain\Explorer\ExplorerState;
use AMZ\Domain\Maze\Map;

class ExplorerFactory
{
    public function make(ExplorationDiary $diary, Map $map, ExplorationEvent ...$events): ExplorerAggregate
    {
        $state = new ExplorerState();

        $publishEvent = static function (ExplorationEvent $event) use ($diary, $state): void {
            $state->apply($event);
            $diary->addEvent($event);
        };

        array_map($publishEvent, $events);

        return new Explorer($state, $map, $publishEvent);
    }
}
