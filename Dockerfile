FROM php:8.0.6-alpine3.13

RUN apk add make

COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /mnt
COPY composer.json composer.json

RUN adduser -DH -u 64 dev

RUN chown -R dev .

USER 64
RUN composer install -q -n --prefer-dist

COPY . .
